using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;

namespace DynamicGroup
{
    class Program
    {
        static void Main(string[] args)
        {
            // The code provided will print ‘Hello World’ to the console.
            // Press Ctrl+F5 (or go to Debug > Start Without Debugging) to run your app.
            List<dtoMyAlbum> listAlbums = new List<dtoMyAlbum>();
            Random rd = new Random();
            for (int i = 0; i < 500; i++)
            {
                var thuocId = rd.Next(1, 5);
                var LoSx = rd.Next(1, 3);
                dtoMyAlbum album = new dtoMyAlbum
                {
                    StrTenThuoc = "Thuoc "+ thuocId,
                    LoSanXuat = LoSx.ToString(),
                    NguonThuoc = i % 3 == 0 ? "BHYT" : "VIENTRO",

                    TenThuocId = thuocId,
                    SoXuat = 23.23+i,
                    TonCuoiKy=10+ i,
                };
                listAlbums.Add(album);
            }
            var groupLoSanXuat = true;
            var groupNguonThuoc = true;

            var _groupBy = "StrTenThuoc,";
            var _select = "it.Key.StrTenThuoc,";
            if (groupLoSanXuat)
            {
                _groupBy += "LoSanXuat,";
                _select += "it.Key.LoSanXuat,";
            }
            if (groupNguonThuoc)
            {
                _groupBy += "NguonThuoc,";
                _select += "it.Key.NguonThuoc,";
            }
            //var _groupBy = "new(StrTenThuoc,LoSanXuat,NguonThuoc)";
            //var _select = "it.Key.StrTenThuoc,it.Key.LoSanXuat,it.Key.NguonThuoc";
            IQueryable<dtoMyAlbum> mydata = listAlbums.AsQueryable();
            var mydataGrouped = mydata.GroupBy("new("+_groupBy.TrimEnd(',') + ")", "it").Select($@"new ({_select.TrimEnd(',')} , SUM(TonCuoiKy) as TonCuoiKy, it as ChiTiet)");

            foreach (dynamic group in mydataGrouped) 
            {
                foreach (dynamic album in group.ChiTiet)
                {
                    Console.WriteLine(album.StrTenThuoc + "_"+ album.LoSanXuat + "_"+ album.NguonThuoc);
                }
            }

            Console.WriteLine("Hello World!");
            Console.ReadKey();

            // Go to http://aka.ms/dotnet-get-started-console to continue learning how to build a console app! 
        }
    }
    public class dtoMyAlbum
    {
        public string StrTenThuoc { get; set; }
        public int TenThuocId { get; set; }
        public string LoSanXuat { get; set; }
        public double SoXuat { get; set; }
        public double TonCuoiKy { get; set; }
        public string NguonThuoc { get; set; }
    }
}
